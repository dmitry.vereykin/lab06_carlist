import java.text.DecimalFormat;

/**
 * Created by Dmitry Vereykin on 10/20/2015.
 */
public class Car {
    String year;
    String make;
    String model;
    int price;

    public Car(){
        this.year = "";
        this.make = "";
        this.model = "";
        this.price = 0;
    }

    public Car(String year, String make , String model , int price) {
        this.year = year;
        this.make = make;
        this.model = model;
        this.price = price;
    }

    public String getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public int getPrice() {
        return price;
    }

}
